package main

import (
	"html/template"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
)

type candidates struct {
	ImageURL   string
	Username   string
	Votes      string
	ProfileURL string
}

type listCandidates struct {
	Candidates []candidates
}

type profile struct {
	Username string
	Votes    string
	Bio      string
	Video    string
	Avatar   string
}

func main() {
	http.HandleFunc("/", route)
	http.ListenAndServe(":8080", nil)
}

func route(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseGlob("templates/*.html")

	switch {
	case r.URL.Path == "/":
		t.ExecuteTemplate(w, "index.html", nil)
	case r.URL.Path == "/about":
		t.ExecuteTemplate(w, "about.html", nil)
	case r.URL.Path == "/candidates":
		t.ExecuteTemplate(w, "candidates.html", getCandidates())
	case strings.HasPrefix(r.URL.Path, "/p/"):
		user := strings.Split(r.URL.Path, "/")[2]
		t.ExecuteTemplate(w, "profile.html", getProfileData(user))
	default:
		t.ExecuteTemplate(w, "404.html", nil)
	}
}

func getCandidates() listCandidates {
	resp, err := http.Get("https://misspuppyuk.com/candidates")
	if err != nil {
		return listCandidates{}
	}
	defer resp.Body.Close()

	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return listCandidates{}
	}
	responseString := string(responseData)

	var o listCandidates

	re := regexp.MustCompile(`<div class="col-lg-4"><img class="img-circle" src="(.+?)" alt="Avatar" width="140" height="140"><h2>(.+?)<\/h2><p>To learn more about this pup, click the button below!<br>This pup has (.+?) Vote\(s\)\.<\/p><p><a class="btn btn-default" href="(.+?)" role="button">View profile<\/a><\/p><\/div>`)

	m := re.FindAllStringSubmatch(responseString, -1)
	for _, n := range m {
		o.Candidates = append(o.Candidates, candidates{ImageURL: n[1], Username: n[2], Votes: n[3], ProfileURL: n[4]})
	}

	return o
}

func getProfileData(user string) profile {
	resp, err := http.Get("https://misspuppyuk.com/p/" + user)
	if err != nil {
		return profile{}
	}
	defer resp.Body.Close()

	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return profile{}
	}
	responseString := string(responseData)

	re := regexp.MustCompile(`(?s)<p class="lead">(.+?)\n?<\/p>`)
	bio := re.FindStringSubmatch(responseString)[1]
	re = regexp.MustCompile(`<h2>(.+?)<\/h2>`)
	username := re.FindStringSubmatch(responseString)[1]
	re = regexp.MustCompile(`<h1>Votes: (.+?)<\/h1>`)
	votes := re.FindStringSubmatch(responseString)[1]
	re = regexp.MustCompile(`<source src="(.+?)" type="video\/mp4">`)
	video := re.FindStringSubmatch(responseString)[1]
	re = regexp.MustCompile(`<img class="img-circle" width="140" height="140" class="featurette-image img-responsive center-block" src="(.+?)" alt="(.+?)'s Avatar"><br><br>`)
	avatar := re.FindStringSubmatch(responseString)[1]

	return profile{Bio: bio, Username: username, Votes: votes, Video: video, Avatar: avatar}
}
